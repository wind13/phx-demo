defmodule FizzBuzzTest do
  use ExUnit.Case
  doctest FizzBuzz
  # use PhxDemoWeb.ConnCase

  test "1 got 1: " do
    assert FizzBuzz.call_out(1) == "1"
  end

  test "3 got Fizz: " do
    assert FizzBuzz.call_out(3) == "Fizz"
  end

  test "5 got Buzz: " do
    assert FizzBuzz.call_out(5) == "Buzz"
  end

  test "15, 51 got FizzBuzz: " do
    assert FizzBuzz.call_out(15) == "FizzBuzz"
    assert FizzBuzz.call_out(51) == "FizzBuzz"
  end

  test "to_str: " do
    assert FizzBuzz.to_str(3) == "3"
  end

  test "is contain" do
    assert FizzBuzz.is_contain(13, 3) == true
  end

  test "13 got Fizz: " do
    assert FizzBuzz.call_out(13) == "Fizz"
  end

  test "7 got Muzz: " do
    assert FizzBuzz.call_out(7) == "Muzz"
  end

  test "73 got FizzMuzz" do
    assert FizzBuzz.call_out(73) == "FizzMuzz"
    assert FizzBuzz.call_out(72) == "FizzMuzz"
    assert FizzBuzz.call_out(75) == "FizzBuzzMuzz"
  end

  test "sub call" do
    assert FizzBuzz.sub_call(3, 3) == "Fizz"
    assert FizzBuzz.sub_call(5, 5) == "Buzz"
    assert FizzBuzz.sub_call(7, 7) == "Muzz"
    assert FizzBuzz.sub_call(17, 7) == "Muzz"
    assert FizzBuzz.sub_call(15, 3) == "Fizz"
    assert FizzBuzz.sub_call(15, 5) == "Buzz"
  end

  test "fizz buzz maps" do
    assert FizzBuzz.number_maps[3] == "Fizz"
  end
end
