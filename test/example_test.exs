defmodule ExampleTest do
  use ExUnit.Case
  doctest Example

  test "greets the world" do
    assert Example.hello("Simon") == "Hello Simon.|SimonSimonSimon"
  end
end
