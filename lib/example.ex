defmodule Example do
  def hello(name) do
    rp = String.duplicate(name, 3)
    "Hello #{name}." <> "|" <> rp
  end
end
